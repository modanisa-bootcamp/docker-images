FROM amazon/aws-cli
ENV KUBECTL_VERSION=v1.18.8

RUN curl -LO "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" 
RUN mv kubectl /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/kubectl


ENTRYPOINT ["/usr/bin/env"]
